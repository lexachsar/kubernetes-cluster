# Sources

1. This repository is based on [Kubernetes Setup Using Ansible and Vagrant](https://kubernetes.io/blog/2019/03/15/kubernetes-setup-using-ansible-and-vagrant/) article.

2. Created cluster uses [flannel](https://github.com/coreos/flannel#flannel) network.


# Pre-Requisites

1. [Vagrant](https://www.vagrantup.com/docs/installation/)
2. [VirtualBox](https://www.virtualbox.org/)
3. VirtualBox Vagrant [provider](https://www.vagrantup.com/docs/providers/)
  ```
  vagrant plugin install virtualbox
  ```
4. [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) or Linux virtual machine with [ansible installed](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

# Usage

1. Clone this repository

2. Move into repository folder and run:
  ```
  cd /path/to/cloned/repository
  vagrant up
  ```
  This command creates 4 virtual machines based on Ubuntu 16.04 image with following credentials:
  - Username: `vagrant`
  - Password: `vagrant`

3. Move into kubernetes-setup folder and run following ansible playbooks:
  ```
  cd kubernetes-setup
  ansible-playbook -i hosts master-playbook.yml --ask-pass --ask-become-pass
  ansible-playbook -i hosts node-playbook.yml --ask-pass --ask-become-pass
  ```
  Use password `vagrant` for ssh login and sudo execution.

## Tips

If something goes wrong, you can destroy vagrant cluster:
`vagrant destroy -f`
